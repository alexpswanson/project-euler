def run():
    fibList = [0,1]
    iter = fibList[0] + fibList[1]
    sum = 0

    while iter <= 4000000:
        iter = iter + fibList[-2]
        fibList.append(iter)

        if(iter % 2 == 0):
            sum += iter
    
    print(sum)

if __name__ == "__main__":
    run()