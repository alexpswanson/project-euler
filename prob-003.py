from utils.timer import Timer

def factorize(target, factors):
  for x in range(2, target):
    if(target % x == 0):
      factors.append(x)
      factors = factorize(int(target/x), factors)
      return factors
  factors.append(target)
  return factors

def run():
  target = 600851475143
  factors = []
  t = Timer()
  t.start()

  factors = factorize(target, factors)

  factors.sort()
  print(factors)
  t.stop()

if __name__ == "__main__":
    run()