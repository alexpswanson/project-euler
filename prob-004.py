from utils.timer import Timer

def isPalindrome(numStr):
  for x in range(0, len(numStr)):
    if numStr[x] != numStr[len(numStr)-1-x]:
      return False
  
  return True

def run():
  upper = 999
  bottom = 99
  palindrome = (0, 0, 0)

  t = Timer()
  t.start()

  for i in range(bottom, upper):
    for j in range(bottom, upper):
      if (i * j) == int(str(i*j)[::-1]) and palindrome[2] < i*j:
      # if isPalindrome(str(i*j)) and palindrome[2] < i*j:
        t.lap()
        palindrome = (i, j, i*j)

  print(f"{palindrome[0]} x {palindrome[1]} = {palindrome[2]}")
  t.stop()

if __name__ == "__main__":
  run()