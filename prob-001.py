def run():
    mult = []
    for x in range(1000):
        if x % 5 == 0 or x % 3 == 0:
            mult.append(x)

    print(sum(mult))

if __name__ == "__main__":
    run()